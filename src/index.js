import phone_field from './telephone.js';
import Geocoder from './geocoder.js';
import { countryChangeHandler, postalCodeChangeHandler } from './cp-country.js';

var objs = {phone_field, Geocoder, countryChangeHandler, postalCodeChangeHandler};
// export for browser
if (typeof window === 'object') {
    for (let [name, obj] of Object.entries(objs)) {
        window[name] = obj;
    }
}

// CJS exports
if (typeof module === 'object' && 'exports' in module) {
    module.exports = { phone_field, Geocoder, countryChangeHandler, postalCodeChangeHandler };
}

// ES6 exports
export { phone_field, Geocoder, countryChangeHandler, postalCodeChangeHandler };
