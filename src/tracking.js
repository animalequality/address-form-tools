/**
 * Send google tag manager technical log
 *
 * @param status 'success' || 'error'
 * @param message The log message
 * @param data Additional variables to be set
 */
const log = (source, status, message, data = {}) => {
    if (Array.isArray(window.dataLayer)) {
        window.dataLayer.push(Object.assign({
            event: 'log',
            logSource: source,
            logStatus: status,
            logMessage: message
        }, data));
    }
}

export {log};
