import postalcodes from './postal-code.json';
import ar_provinces from './ar-provinces.json';
import Geocoder from './geocoder.js';
import {log} from './tracking.js';

const geocoder = new Geocoder();

function httpstatus(response) {
    if (response.status >= 200 && response.status < 300) {
	return Promise.resolve(response);
    } else {
	return Promise.reject(new Error(response.statusText));
    }
}

function validateCountry(country, cp) {
    if (! country) return;

    let cc_opt = country.options[country.selectedIndex];
    if (! cc_opt) {
        console.warn("didn't identified a valid country");
        return;
    }
    let cc = cc_opt.value;
    if (typeof telinput_field !== 'undefined' && typeof telinput_field.isEmpty === 'function') {
        /**
         * Only change the telephone country flag if no value
         * existed in this field (people address in country X but
         * having filled a phone number from country X)
         */
        if (telinput_field.isEmpty()) {
            telinput_field.setCountry(cc.toLowerCase());
        }
    }

    if (cc.length != 2 || ! (cc in postalcodes)) {
        country.setCustomValidity('');
    }

    // validate postal code
    if (! cp) {
        return;
    }
    if (cp.value == "" && !cp.required) {
        cp.setCustomValidity('');
        return;
    }

    if (! (cc in postalcodes)) {
        return;
    }

    cp.value = cp.value.trim();
    let [human_fmt, cp_regexp] = postalcodes[cc];
    if (!cp.value.match(cp_regexp)) {
        if (cc == 'MX' && cp.value.length == 4) {
            cp.setCustomValidity('El código postal debe tener 5 dígitos. Agregar 0 al principio para completar tu código postal.');
            return;
        }
        if (cc == 'AR' && cp.value.length == 4) {
            if (! (cp.value in ar_provinces)) {
                cp.setCustomValidity('Non-existing postal code');
                return;
            }
            var region_s = ar_provinces[cp.value];
            if (region_s.length == 1) {
                cp.value = region_s + cp.value;
                cp.setCustomValidity('');
                return;
            }

            cp.setCustomValidity("This Argentinan postal-code matches several states' letters: " + region_s);
            return;
        }

        cp.setCustomValidity('Invalid postal code format. Expected ' + human_fmt);
        if (cp.value) {
            log('addressTools', 'error', 'invalid postal code', {logData: `${cp.value} in ${cc} with ${cp_regexp}`});
        }

        // please stop here.
        return;
    }

    // Otherwise, just reset validity.
    country.setCustomValidity('');
    cp.setCustomValidity('');
}

function handleCountryState(country, cp_element) {
    if (!postalcodes || !cp_element || !country || !cp_element.value) return;
    var cc_element = country.options[country.selectedIndex];

    if (! cc_element || ! ('value' in cc_element)) {
        return;
    }
    if (! (cc_element.value in postalcodes) || !cp_element.validity.valid) {
        // reset hint field
    }
    else {
        document.dispatchEvent(new CustomEvent('geocode:start'));
        geocoder.geocode(cc_element.value,
                         cp_element.value,
                         {fields: ['admin1', 'city']})
            .then((e) => {
                console.log(e);
                document.dispatchEvent(new CustomEvent('geocode:geocoded', { detail: e }));
                return e;
            })
            .finally(() => document.dispatchEvent(new CustomEvent('geocode:end')));
    }
}

function togglePostalCode(country, cp) {
    if (!postalcodes || !cp || ! country) return;
    var cc_element = country.options[country.selectedIndex];
    if (! cc_element) {
        return;
    }

    var cc = cc_element.value;
    if (! (cc in postalcodes)) {
        document.dispatchEvent(new CustomEvent('address-form-tools:has-postal-code', { detail: { show: false, cc, value: '00000' } }));
        cp.value = '00000';
        cp.readOnly = true;
    } else {
        let [fmt, re] = postalcodes[cc];
        cp.readOnly = false;
        if (cp.value == '00000') {
            cp.value = '';
        }
        document.dispatchEvent(new CustomEvent('address-form-tools:has-postal-code', { detail: { show: true, cc, fmt, re } }));
    }
}

/**
 * @param country An existing DOM node or selector.
 * @param cp      An existing DOM node or selector.
 */
function countryChangeHandler(country, cp) {
    if (typeof country === 'string') {
        country = document.querySelector(country);
    }
    if (typeof cp === 'string') {
        cp = document.querySelector(cp);
    }

    validateCountry(country, cp);
    togglePostalCode(country, cp);
    handleCountryState(country, cp);
    document.dispatchEvent(new CustomEvent('jsdom:country-changed'));
}

/**
 * @param country An existing DOM node or selector.
 * @param cp      An existing DOM node or selector.
 */
function postalCodeChangeHandler(country, cp) {
    if (typeof country === 'string') {
        country = document.querySelector(country);
    }
    if (typeof cp === 'string') {
        cp = document.querySelector(cp);
    }

    validateCountry(country, cp);
    handleCountryState(country, cp);
    document.dispatchEvent(new CustomEvent('jsdom:postalcode-changed'));
}

if (typeof module !== 'undefined') {
    module.exports = {countryChangeHandler, postalCodeChangeHandler};
}
export {countryChangeHandler, postalCodeChangeHandler};
