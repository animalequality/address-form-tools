import intlTelInput from 'intl-tel-input';
import examples from 'libphonenumber-js/examples.mobile.json';
import { /* AsYouType, */ parsePhoneNumber, getExampleNumber } from 'libphonenumber-js/max';

var t = null;
const errorMap = [ "", "Invalid country code", "Too short", "Too long", "Invalid number"];

function get_country_data(c) {
    if (! c) {
        return {};
    }

    var preferred = [],
        only = [],
        stop = false;
    for(const i of c.options) {
        if (i.value == 'ez') {
            continue;
        }
        if (i.value == '--') {
            stop = true;
            continue;
        }
        only.push(i.value);
        if (!stop) {
            preferred.push(i.value);
        }
    }

    if (!stop) {
        preferred = [];
    }
    if (only > 100) {
        only = null;
    }

    var current = c.options[c.selectedIndex];
    if (current && current.value && current.value.length == 2);
    current = current.value.toLowerCase();
    return { only, preferred, current };
}

function validateCountry(elTelInput, element, check_validity = true) {
    if (!elTelInput || !elTelInput.getSelectedCountryData()) {
        return;
    }

    const iso = elTelInput.getSelectedCountryData().iso2.toUpperCase() || '';
    if (!iso) {
        return;
    }

    const p = getExampleNumber(iso, examples);
    element.placeholder = p.formatNational();
    if (! element.required && ! element.value) {
        // If phonenumber is not required, and not value is set
        // then just don't set an error message
        element.setCustomValidity('');
        return;
    }

    try {
        const phoneNumber = parsePhoneNumber(element.value, iso);
        elTelInput.setNumber(phoneNumber.formatInternational());
        if (check_validity) {
            element.setCustomValidity(phoneNumber.isValid() ? '' : 'Invalid');
        }
    } catch(error) {
        if (check_validity) {
            element.setCustomValidity(error);
        }
    }
}

export default function (element, country_element) {
    if (! element) {
        return;
    }

    const { only = null, preferred = [], current = "" } = get_country_data(country_element);
    t = window.telinput_field = intlTelInput(element, {
        initialCountry: current,
        preferredCountries: preferred,
        onlyCountries: only,
        formatOnDisplay: false,
        // nationalMode: false,
        // Needed for dynamic placeholder but heavier than libphonenumber-js/max'
        // utilsScript: 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/15.1.0/js/utils.js'
    });

    t.isEmpty = function() { return (!this.telInput || !this.telInput.value) && (!this.a || !this.a.value); }.bind(t);
    t.promise.then(() => validateCountry(t, element, false));
    element.addEventListener("countrychange", () => validateCountry(t, element));
    element.addEventListener("blur", () => validateCountry(t, element));
};
