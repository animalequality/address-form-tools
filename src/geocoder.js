/*
 * This file is part of the Cygne sync bundle
 * and was manually translated to ES6.
 *
 * (c) 2019, Raphaël Droz <raphael@droz.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

function httpstatus(response) {
    if (response.status >= 200 && response.status < 300) {
	return Promise.resolve(response);
    } else {
	return Promise.reject(new Error(response.statusText));
    }
}

function http_build_query(data) {
    var p = new URLSearchParams();
    Object.keys(data).forEach(function(name) {
        p.append(name, data[name]);
    });
    return p.toString();
}

class Geocoder
{
    constructor(options = {})
    {
        /**
         * All known providers
         */
        this.KNOWN_PROVIDERS = ['opencagedata', 'nominatim', 'zippopotam', 'geonames', 'google', 'cepaberto'];

        /**
         * Default providers if none were set in config/services.yaml and passed to this class instance
         * Those not having their API configured will be ignored.
         */
        this.default_providers = ['cepaberto', 'google', 'opencagedata', 'nominatim', 'geonames', 'zippopotam'];

        this.RETURN_ADMIN1 = 1;
        this.RETURN_CITY   = 1 << 2;
        this.RETURN_BOTH   = this.RETURN_ADMIN1 | this.RETURN_CITY;

        if (options.default_providers) {
            this.default_providers = options.default_providers;
        }
    }

    /**
     * @param options An array of option for this geocoding request.
     *  - providers A list of provider's names
     *  - max_requests Do request more than that many services
     *
     * @return Either the city or admin1 as string
     *         Either an object containing known level 1 administrative zone and city.
     *         Either null if not found in any of the geocoder services.
     */
    async geocode(country, postalcode, options = {})
    {
        const providers = this.getProviders(options.providers || options.p || false),
              max_requests = options.max_requests || this.KNOWN_PROVIDERS.length,
              return_fields = options.return || options.fields || ['admin1'],
              return_fields_value = this.returnFieldsAsInt(return_fields);

        var i = 0, ret = false;
        while (!ret && providers[i]) {
            var p = providers[i++],
                data = [p, i, providers.length, country, postalcode];
            try {
                ret = await this.geocodeWith(p, country, postalcode, return_fields_value);
                if (ret) {
                    data.push(ret);
                    console.log('%s geocoder (%d/%d): %s/%s success: "%s"', ...data);
                    console.log(ret);
                    return ret;
                } else {
                    console.warn('%s geocoder (%d/%d): %s/%s failed', ...data);
                }
            } catch(e) {
                data.push(e.name);
                console.warn('%s geocoder (%d/%d): %s/%s exception: %s', ...data);
                console.warn(e);
            }
        }
        document.dispatchEvent(new CustomEvent('geocode:failed', { detail: data }));
        return false;
    }

    returnFieldsAsInt(arr) {
        var ret = 0;
        if (arr.indexOf('admin1') >= 0) {
            ret |= this.RETURN_ADMIN1;
        }
        if (arr.indexOf('city') >= 0) {
            ret |= this.RETURN_CITY;
        }
        return ret;
    }

    geocodeWith(provider, country, postalcode, fields)
    {
        var args = [country, postalcode, fields || this.RETURN_ADMIN1],
            auth = this.getProviderAuthToken(provider);
        if (auth === false) {
            return false;
        } else if (auth) {
            args.push(auth);
        }
        // grep-friendly: this.(opencagedata|nominatim|geonames|google|zippopotam)(...)
        return this[provider](...args);
    }

    /**
     * Return credentials parameters for a given geocoding provider.
     * @return
     *  - false if this provider is unusable (mandatory but absent credentials)
     *  - null  if credentials are absent but optional
     *  - array(name => value) parameter(s) to be used as credentials
     *
     * Google availability is mostly about google object presence
     * OpenCageData mandates credentials.
     * Nominatim and Geonames have optional credentials.
     * Zippopotam does not need credentials.
     */
    getProviderAuthToken(name)
    {
        var k;
        if (name === 'google') {
            if (this.isNode()) {
                k = this.getcnf('GOOGLE_API');
                return k ? {key: k} : false;
            } else {
                return typeof google !== 'undefined' && google ? null : false;
            }
        }
        if (name === 'opencagedata') {
            k = this.getcnf('OPENCAGEDATA_API');
            return k ? {key: k} : false;
        }
        if (name === 'geonames') {
            k = this.getcnf('GEONAMES_USER');
            return k ? {username: k} : null;
        }
        if (name === 'nominatim') {
            k = this.getcnf('NOMINATIM_EMAIL');
            return k ? {email: k} : null;
        }
        if (name === 'cepaberto') {
            k = this.getcnf('CEPABERTO_TOKEN');
            return k ? {token: k} : false;
        }
        return null;
    }

    isNode() {
        return typeof process !== 'undefined'
            && typeof process.versions !== 'undefined'
            && process.env && process.versions.node;
            // && typeof module !== 'undefined' && module.exports // Not true anymore with ESM version of node/mocha
    }

    getcnf(v) {
        var auth = typeof window !== 'undefined' && 'geocoding_auth' in window ? window.geocoding_auth[v] : this.isNode() ? process.env[v] : null;
        return auth;
    }

    /**
     * Validate chosen geocoding providers or provide them from a default list if none were requested.
     * If specifically requests provider(s) are unavailable (eg: Google or OpenCageData without their API keys)
     * then default ones are used.
     *
     * @param providers A string, or an array of string representing providers to be used for a geocoding request.
     * @return A list of usable providers.
     */
    getProviders(providers)
    {
        var specific = !!providers,
            ret = [];
        providers = providers === '*' ? this.KNOWN_PROVIDERS : (
            typeof providers === 'string' ? [providers] : (
                providers ? providers : this.default_providers
            )
        );

        for(var k of providers) {
            var name = k.toLowerCase();
            if (this.KNOWN_PROVIDERS.indexOf(name) === -1) {
                throw new Error("Unknown geocoding provider $name");
            }
            if (this.getProviderAuthToken(name) !== false) {
                ret.push(name);
            }
        }

        if (!ret && specific) {
            return this.getProviders(this.default_providers);
        }

        return ret.filter((v, i, a) => a.indexOf(v) === i); // unique
    }

    cepaberto(country, postalcode, fields, auth)
    {
        if (country.toLowerCase() !== 'br') {
            return false;
        }
        var url    = 'https://www.cepaberto.com/api/v3/cep',
            params = {cep: this.postalCodeMod(country, postalcode, 'cepaberto')},
            headers = new Headers(),
            retFields = (r) => {
                switch(fields) {
                case this.RETURN_ADMIN1: return r.estado ? r.estado.sigla : null;
                case this.RETURN_CITY: return r.cidade ? r.cidade.nome : null;
                default: return { admin1: r.estado ? r.estado.sigla : null, city: r.cidade ? r.cidade.nome : null };
                }
            };

        headers.set('Authorization', `Token token=${auth.token}`);
        url += '?' + http_build_query({...params});
        return fetch(url, {headers}).then(httpstatus).then(r => r.json());
    }

    opencagedata(country, postalcode, fields, auth)
    {
        var url    = 'https://api.opencagedata.com/geocode/v1/json',
            params = {countrycode: country,
                      q: postalcode,
                      limit: 1},
            retFields = (r) => {
                switch(fields) {
                case this.RETURN_ADMIN1: return r.state;
                case this.RETURN_CITY: return r.city || r.county || r.region;
                default: return { admin1: r.state, city: r.city || r.county || r.region };
                }
            };

        url += '?' + http_build_query({...params, ...auth});
        return fetch(url).then(httpstatus).then(r => r.json())
            .then(r => 'results' in r && r.results.length ? retFields(r.results[0].components) || null : null);
    }

    nominatim(country, postalcode, fields, auth = {})
    {
        var url    = 'https://nominatim.openstreetmap.org/search',
            params = {countrycodes: country,
                      postalcode: this.getPostalPrefix(country, postalcode),
                      addressdetails: 1,
                      format: 'json'},
            retFields = (r) => {
                switch(fields) {
                case this.RETURN_ADMIN1: return r.state;
                case this.RETURN_CITY: return r.city || r.town || r.village || r.county || r.city_district;
                default: return { admin1: r.state, city: r.city || r.town || r.village || r.county || r.city_district };
                }
            };

        url += '?' + http_build_query({...params, ...auth});
        return fetch(url).then(httpstatus).then(r => r.json())
            .then(r => r.length ? retFields(r[0].address) || null : null);
    }

    /**
     * Note that while postalCodeSearchJSON contains `postalCodes`,
     * postalCodeLookupJSON contains `postalcodes` (lowercase)
     */
    geonames(country, postalcode, fields, auth = {})
    {
        var url    = 'https://secure.geonames.org/postalCodeSearchJSON',
            params = {country: country, postalcode: postalcode},
            retFields = (r) => {
                switch(fields) {
                case this.RETURN_ADMIN1: return r.adminName1;
                case this.RETURN_CITY: return r.placeName;
                default: return { admin1: r.adminName1, city: r.placeName };
                }
            };

        url += '?' + http_build_query({...params, ...auth});
        return fetch(url).then(httpstatus).then(r => r.json())
            .then(r => 'postalCodes' in r && r.postalCodes.length ? retFields(r.postalCodes[0]) || null : null);
    }

    // Wrapper for both the node.js and googlemaps API version of the Geocoder
    google(country, postalcode, fields, auth = false)
    {
        return this.isNode()
            ? this.google_node(country, postalcode, fields, auth)
            : this.google_web(country, postalcode, fields);
    }

    // A common private helper to extract data from the response
    googleGetResult(components, fields) {
        var c,
            ret = {},
            retFields = (r) => {
                switch(fields) {
                case this.RETURN_ADMIN1: return r.admin1;
                case this.RETURN_CITY: return r.city;
                default: return r;
                }
            };

        for(c of components) {
            if (c.types.indexOf('administrative_area_level_1') >= 0) {
                ret['admin1'] = c.long_name;
            }
        }

        var preferred_fields = ['locality', 'sublocality', 'postal_town', 'administrative_area_level_3'];

        loop1:
        for(let field of preferred_fields) {
            for(c of components) {
                if (c.types.indexOf(field) >= 0) {
                    ret['city'] = c.long_name;
                    break loop1;
                }
            }
        }

        return retFields(ret);
    }

    google_web(country, postalcode, fields)
    {
        var g = new google.maps.Geocoder(),
            params = {componentRestrictions: {country: country, postalCode: postalcode}};
        return new Promise((resolve, reject) => {
            g.geocode(params, (r, status) => {
                if (status !== 'OK') {
                    reject(new Error("google: location not found"));
                    return;
                }
                if (!r.length) {
                    reject(new Error("google: no result"));
                    return;
                }
                resolve(this.googleGetResult(r[0].address_components, fields));
            });
        });
    }

    google_node(country, postalcode, fields, auth)
    {
        var url    = 'https://maps.googleapis.com/maps/api/geocode/json',
            params = {components: `country:${country}|postal_code_prefix:${postalcode}`,
                      type: 'json'};
        url += '?' + http_build_query({...params, ...auth});
        return fetch(url).then(httpstatus).then(r => r.json()).then(
            r => {
                if (! ('results' in r) || !r.results.length) {
                    return null;
                }
                return this.googleGetResult(r.results[0].address_components, fields);
            }
        );
    }

    zippopotam(country, postalcode, fields)
    {
        var cp = this.getPostalPrefix(country, postalcode),
            retFields = (r) => {
                switch(fields) {
                case this.RETURN_ADMIN1: return r['state'];
                case this.RETURN_CITY: return r['place name'];
                default: return { admin1: r['state'], city: r['place name'] };
                }
            };
        if (!cp) {
            return null;
        }
        return fetch(`https://api.zippopotam.us/${country}/${postalcode}`).then(httpstatus).then(r => r.json())
            .then(r => 'places' in r && r.places.length ? retFields(r.places[0]) || null : null);
    }

    /**
     * Both nominatim and zippopotam.us only handles postalcode geocoding from a subset of postal codes.
     * For example, only the 3 first characters for Canada or 5 first for Brazil (Nominatim).
     * This function map one to the other
     */
    getPostalPrefix(country, postalcode)
    {
        switch (country) {
        case 'AR':
            return postalcode.replace(/^0*(\d{4}).*/, '$1');
        case 'BR':
            return postalcode.substr(0, 5);
        case 'CA':
            return postalcode.substr(0, 3);
        case 'GR':
        case 'VE':
            // no zippopotam.us
            return null;
        case 'US':
            return postalcode.substr(0, 5);
        default:
            return postalcode;
        }
    }

    postalCodeMod(country, postalcode, service)
    {
        if (service === 'cepaberto') {
            return postalcode.replace('-', '');
        }

        if (country === 'BR') {
            // Mandatory dash for BR postalcode for opencagedata
            if (service === 'opencagedata') {
                return postalcode.replace('/^([0-9]{5})([0-9].*)/', '$1-$2');
            }

            // Geonames needs it stripped to 5 first digits.
            return postalcode.substring(0, 5);
        }

        // Don't over-modify postcodes for opencagedata.
        if (service === 'opencagedata') {
            return postalcode;
        }

        return this.getPostalPrefix(country, postalcode);
    }
}

if (typeof window === 'object') {
    function gm_authFailure() {
        console.log("Google geocoder error, removing to allow fallback to take place");
        if (typeof google !== 'undefined') {
             google = null;
        }
    }
    window.gm_authFailure = gm_authFailure;
}

if (typeof module !== 'undefined') {
    module.exports = Geocoder;
}
export default Geocoder;
