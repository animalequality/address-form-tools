import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import json from 'rollup-plugin-json';
import pkg from './package.json';
import resolve from 'rollup-plugin-node-resolve';
import strip from 'rollup-plugin-strip';
import visualizer from 'rollup-plugin-visualizer';
import { terser } from "rollup-plugin-terser";

const isProduction = process.env.NODE_ENV === 'production';

function plugins(opts = {
    compress: false,
    stats: false,
    stats_w_sm: false
}) {
    return [
        commonjs(),
        json(),
        resolve(),  // {only: ['core-js', '@babel/runtime', 'regenerator-runtime']} ?
        babel({
            babelrc: false,
            exclude: 'node_modules/**',
            runtimeHelpers: true,
            plugins: [
                "@babel/plugin-transform-runtime"
            ],
            presets: [
                [
                    '@babel/preset-env',
                    {
                        corejs: 3,
                        modules: false,
                        useBuiltIns: 'usage',
                        debug: true,
                    },
                ],
            ],
        }),
        opts.compress ? strip() : null,
        opts.compress ? terser() : null,
        opts.stats ? visualizer({sourcemap: opts.stats_w_sm, filename: opts.stats}) : null
    ];
}

export default [
    {
        input: 'src/index.js',
        plugins: plugins(),
        output: [
            { file: pkg.browser, format: 'umd', name: 'address_form_tools', sourcemap: true },
            { file: pkg.module, format: 'es', sourcemap: true },
            // { file: pkg.main, format: 'cjs' },
        ]
    },

    {
        input: 'src/index.js',
        plugins: plugins({compress: true, stats: 'stats.html'}),
        output: [
            { file: pkg.browser.replace(/.js$/, '.dist.js'), format: 'umd', name: 'address_form_tools', sourcemap: true },
        ]
    },

    {
        input: 'src/index.js',
        plugins: plugins({compress: true, stats: 'stats-np.html', stats_w_sm: true}),
        external: ['intl-tel-input', 'libphonenumber-js/max'],
        output: [
            {
                file: pkg.browser.replace(/.js$/, '.nophone.js'), format: 'umd', name: 'address_form_tools', sourcemap: true,
                globals: {
                    // https://github.com/jackocnr/intl-tel-input#getting-started
                    'intl-tel-input': 'intlTelInput',
                    // see https://github.com/catamphetamine/libphonenumber-js/#cdn
                    'libphonenumber-js/max': 'libphonenumber',
                }
            },
            {
                file: pkg.module.replace(/.es6.js$/, '.nophone.es6.js'), format: 'es', sourcemap: true,
                globals: {
                    // https://github.com/jackocnr/intl-tel-input#getting-started
                    'intl-tel-input': 'intlTelInput',
                    // see https://github.com/catamphetamine/libphonenumber-js/#cdn
                    'libphonenumber-js/max': 'libphonenumber',
                }
            }
        ]
    }
];
