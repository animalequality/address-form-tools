/*
 * (c) 2019, Raphaël Droz <raphael@droz.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { assert } from 'chai';
import Geocoder from '../src/geocoder.js';
import fixtures from './fixtures-geocoder.js';
import nf from 'node-fetch';

global['fetch'] = nf;

function testOne(provider, country, postalcode, fields) {
    var g = new Geocoder();
    return g.geocodeWith(provider, country, postalcode, fields);
}

async function testGeocode() {
    var args = Object.values(arguments).slice(0, arguments.length - 1),
        g = new Geocoder();
    var r = g.geocode(...args);
    assert.equal(await r, arguments[arguments.length - 1]);
}

describe('fixtured geocoding', function() {
    it('Using Google', () => {
        var results,
            g = new Geocoder();

        results = g.googleGetResult(
            fixtures.google['it-20814'].results[0].address_components,
            ['city', 'admin1']
        );
        assert.deepEqual(
            results,
            { admin1: 'Lombardy', city: "Varedo" }
        );

        results = g.googleGetResult(
            fixtures.google['fr-20000'].results[0].address_components,
            ['city', 'admin1']
        );
        assert.deepEqual(
            results,
            { admin1: 'Corsica', city: "Ajaccio" }
        );

        results = g.googleGetResult(
            fixtures.google['uk-SE11 5DP'].results[0].address_components,
            ['city', 'admin1']
        );
        assert.deepEqual(
            results,
            { admin1: 'England', city: "London" }
        );

        results = g.googleGetResult(
            fixtures.google['br-24230-090'].results[0].address_components,
            ['city', 'admin1']
        );
        assert.deepEqual(
            results,
            { admin1: 'State of Rio de Janeiro', city: "Icaraí" }
        );

    });
});

describe('geocoding', function() {
    it('Nominatim', async() => {
        var r = await testOne("nominatim", "de", "70178", ['admin1','city']);
        assert.deepEqual(
            r,
            {admin1: "Baden-Württemberg", city: "Stuttgart"}
        );
    });

    it('Zippopotam', async() => {
        assert.deepEqual(
            await testOne("zippopotam", "fr", "88000"),
            "Lorraine"
        );
    });

    it('Geonames', async() => {
        // needs GEONAMES_USER envvar
        assert.deepEqual(
            await testOne("geonames", "ca", "M6P-4J6"),
            "Ontario"
        );
    });

    it('Opencagedata', async() => {
        // needs OPENCAGEDATA_API envvar
        assert.deepEqual(
            await testOne("opencagedata", "fr", "88000"),
            "Grand Est"
        );
    });

    xit('aGoogle', async() => {
        // needs GOOGLE_API envvar. Inconsistent
        assert.deepEqual(
            await testOne("google", "br", "24230-090"),
            "State of Rio de Janeiro"
        );
    });

    it('Specific provider is passed', () => {
        testGeocode("fr", "88000", {p: 'nominatim'}, "Grand Est");
    });

    it('Zippopotam and Not-existing place', () => {
        testGeocode("fr", "notexist", {p: 'zippopotam'}, false);
    });
});
