import { assert } from 'chai';
import { sleep, getFormInfos, fillForm, submit } from './helpers/helpers.js';
import { load, window, document } from './helpers/mocha-soup.js';

describe.skip('email features', function() {
    before(async() => {
        await load({form: 'form-standard.html'});
    });

    after(() => {
        window.close();
    });

    it('email-fixer: Check if correct email address is valid', function() {
        const { form, email } = getFormInfos();
        fillForm(form, {email: 'it@gmail.com'});
        form.checkValidity();
        assert.equal(email.validity.valid, true);
    });

    it('email-fixer: Check if email address with typo gets custom message', function() {
        const { form, email } = getFormInfos();
        fillForm(form, {email: 'it_support@gnail.com'});
        form.checkValidity();

        assert.equal(email.validity.valid, true);
        // TODO This does not work yet. For any reason, "setCustomValidity()" seems not to be called
        //assert.equal(email.validity.customError, true);
        //assert.equal(email.validationMessage, "Email seems mistyped. Suggested: it_support@gmail.com");
    });

    it('email-fixer: Check if incorrect email address is marked as invalid', function() {
        const { form, email } = getFormInfos();
        fillForm(form, {email: 'itgmail.'});
        form.checkValidity();
        assert.equal(email.validity.valid, false);
        fillForm(form, {email: '54321'});
        form.checkValidity();
        assert.equal(email.validity.valid, false);
    });
});

describe('location features', function() {
    before(async() => {
        await load({form: 'form-location.html'});
    });

    after(() => {
        window.close();
    });

    it('Some countries hide postalCode field', function(done) {
        const { form, postalCode } = getFormInfos();

        document.addEventListener('address-form-tools:has-postal-code', (e) => {
            assert.equal(e.detail.show, false);
            done();
        }, { once: true });
        fillForm(form, {country: 'AQ' /* Antartida has no postal code */});
    });

    it('City/State hinting works', function(done) {
        this.timeout(7000);
        const { form } = getFormInfos();

        document.addEventListener('geocode:geocoded', (e) => {
            assert.equal(e.city, "Stuttgart");
            assert.equal(e.admin1, "Baden-Württemberg");
            done();
        }, { once: true });
        fillForm(form, {email: 'blah@localhost.local', country: 'DE', postalCode: '70173'});
    });

    it('A bad postal code trigger a Google Analytics event, #1123610180525890', function(done) {
        const { form } = getFormInfos();

        // assert.exists(description);
        window.ga = function(ga_action = 'send', ga_event = 'event', slug, action, label, value) {
            if (slug === 'cp' && action === 'wrong-postal-code' && label.includes('XXXXXXXXX')) {
                done();
            }
        };
        fillForm(form, {email: 'blah@localhost.local', country: 'DE', postalCode: 'XXXXXXXXX', city: 'foo'});
        submit(form);
    });
});
