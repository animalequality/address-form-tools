import jsdom from 'jsdom';
import path from 'path';
import process from 'process';

const localname = process.env['LOCALHOST'] || 'localhost.local';

// This window "state" means that test suites (describe()) must be run sequentially (which is the case)
var document = null, window = null;

function getFormInfos() {
    var form = document.querySelector("form"),
        country = form.querySelector("select[name=country]"),
        postalCode = form.querySelector("input[name=postalCode]"),
        email = form.querySelector("input[name=email]");
    return { form, country, postalCode, email };
}

function fillForm(form, obj, action = `http://${localname}:3001/sign`) {
    for(let [key, value] of Object.entries(obj)) {
        if (['lastName','firstName','country','city','postalCode','email'].indexOf(key) === -1) {
            continue;
        }
        var field = form.querySelector("[name=" + key + "]");
        if (field && value !== undefined) {
            console.log("blurring field " + key);
            field.value = value;
            // ToDo: here the event "change" event is expected to be sent
            // let evt = new window.KeyboardEvent('keydown', { key: 'Tab' });
            // field.dispatchEvent(new window.Event("change"));
            field.blur();
        }
    }

    form.action = action;
}

/**
 * form.submit() will not trigger validation as a click simulation would
 * Because, sadly, even simulating a click on an invalid form makes JSDom
 * attempt to submit it.
 */
function submit(obj) {
    const button = obj.querySelector('[type=submit]');
    if (obj.checkValidity()) {
        button.click();
        return true;
    } else {
        var report = {'main': obj.reportValidity()};
        obj.querySelectorAll('input,select').forEach(v => report[v.id] = v.reportValidity());
        return report;
    }
}

function sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}

/**
 * Load a piece of user-provided HTML using JSDom and update
 * the (global) properties of this module.
 * @param html_file javascript path, relative to project root.
 */
async function load_html(html_file, url = `http://${localname}:3002/`) {
    const my_jsdom = await jsdom.JSDOM.fromFile(`${html_file}`, { url: url,
                                                                  contentType: "text/html; charset=utf-8",
                                                                  resources: "usable",
                                                                  runScripts: "dangerously" });
    window = my_jsdom.window.document.defaultView;
    document = my_jsdom.window.document;
}

export { window, document, sleep, fillForm, load_html, localname, getFormInfos, submit };
