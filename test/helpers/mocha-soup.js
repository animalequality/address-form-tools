/**
 * Problem of this solution
 * jsdom does not implement window.unload (https://github.com/jsdom/jsdom/issues/1584)
 *   which would allow to run after() hook in a synchronous fashion and avoid races accross Mocha testsuites.
 */
import process from 'process';
import fs from 'fs';
import { dirname } from 'path';
import { fileURLToPath } from 'url';
import { createInstrumenter } from 'istanbul-lib-instrument';
import { window, document, load_html } from './helpers.js';

const testdir =  dirname(dirname(fileURLToPath(import.meta.url)));

// A path where instrumented code have been generated before running `npm test`, using
// npm run instrument
const project_dir = dirname(testdir);
const nyc_dir = dirname(testdir) + '/nyc';

/**
 * Simply loads a script as a browser would from Grav HTML output.
 * loading takes time (https://github.com/jsdom/jsdom#asynchronous-script-loading)
 * and we before() test setup needs to be pending until our script actually loaded.
 */
function load_script(script_path) {
    var fullpath = project_dir + '/' + script_path;
    if (process.env['NYC_CWD']) {
        fullpath = nyc_dir + '/' + script_path;
        // A more costly alternative would be to generate instremented code at runtime.
        // var instrumenter = createInstrumenter(), js = instrumenter.instrumentSync(js, fullpath);
    }

    var js = fs.readFileSync(fullpath, { encoding: 'utf-8' });
    return load_script_content(js);
}

function load_script_content(js, head = true) {
    return new Promise((resolve, reject) => {
        const scriptEl = window.document.createElement('script');
        scriptEl.textContent = js;
        scriptEl.onload = resolve;
        scriptEl.onerror = reject;
        if (head) {
            document.head.appendChild(scriptEl);
        } else {
            document.body.appendChild(scriptEl);
        }
    });
}

/**
 * JSDOM does not implement fetch (https://github.com/jsdom/jsdom/issues/1724)
 * but Response is not part of the global namespace anyway (not usable for MailChimp JSONP)
 */
function load_polyfills() {
    const scriptEl = window.document.createElement('script');
    scriptEl.src = 'https://unpkg.com/unfetch/polyfill';
    window.document.head.appendChild(scriptEl);
}

async function load({form, script_path = 'build/bundle.js'}) {
    await load_html(testdir + '/' + form);
    load_polyfills();
    await load_script(script_path);
    await load_script_content(
        `
const c = form.querySelector("form select[name=country]"),
      p = form.querySelector("form input[name=postalCode]");

if (c) {
    c.onchange = countryChangeHandler;
}
if (p) {
    p.onchange = postalCodeChangeHandler;
}
`, false);
}

export { window, document, load, load_script };
