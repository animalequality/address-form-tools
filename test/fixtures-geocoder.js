let google = {
    "it-20814": {
	results: [
	    {
		"address_components" : [
		    {
			"long_name" : "20814",
			"short_name" : "20814",
			"types" : [ "postal_code" ]
		    },
		    {
			"long_name" : "Varedo",
			"short_name" : "Varedo",
			"types" : [ "administrative_area_level_3", "political" ]
		    },
		    {
			"long_name" : "Province of Monza and Brianza",
			"short_name" : "MB",
			"types" : [ "administrative_area_level_2", "political" ]
		    },
		    {
			"long_name" : "Lombardy",
			"short_name" : "Lombardy",
			"types" : [ "administrative_area_level_1", "political" ]
		    },
		    {
			"long_name" : "Italy",
			"short_name" : "IT",
			"types" : [ "country", "political" ]
		    }
		],
		"types" : [ "postal_code" ]
	    }
	],
    },

    "fr-20000": {
        "results" : [
            {
                "address_components" : [
                    {
                        "long_name" : "20000",
                        "short_name" : "20000",
                        "types" : [ "postal_code" ]
                    },
                    {
                        "long_name" : "Ajaccio",
                        "short_name" : "Ajaccio",
                        "types" : [ "locality", "political" ]
                    },
                    {
                        "long_name" : "Corse-du-Sud",
                        "short_name" : "Corse-du-Sud",
                        "types" : [ "administrative_area_level_2", "political" ]
                    },
                    {
                        "long_name" : "Corsica",
                        "short_name" : "Corsica",
                        "types" : [ "administrative_area_level_1", "political" ]
                    },
                    {
                        "long_name" : "France",
                        "short_name" : "FR",
                        "types" : [ "country", "political" ]
                    }
                ],
                "formatted_address" : "20000 Ajaccio, France",
                "types" : [ "postal_code" ]
            }
        ],
    },
    
    "uk-SE11 5DP": {
        results: [
            {
                "address_components" : [
                    {
                        "long_name" : "SE11 5DP",
                        "short_name" : "SE11 5DP",
                        "types" : [ "postal_code" ]
                    },
                    {
                        "long_name" : "London",
                        "short_name" : "London",
                        "types" : [ "postal_town" ]
                    },
                    {
                        "long_name" : "Greater London",
                        "short_name" : "Greater London",
                        "types" : [ "administrative_area_level_2", "political" ]
                    },
                    {
                        "long_name" : "England",
                        "short_name" : "England",
                        "types" : [ "administrative_area_level_1", "political" ]
                    },
                    {
                        "long_name" : "United Kingdom",
                        "short_name" : "GB",
                        "types" : [ "country", "political" ]
                    }
                ],
                "formatted_address" : "London SE11 5DP, UK",
                "types" : [ "postal_code" ]
            }
        ],
    },

    "br-24230-090": {
        results: [
            {
                "address_components" : [
                    {
                        "long_name" : "24230-090",
                        "short_name" : "24230-090",
                        "types" : [ "postal_code" ]
                    },
                    {
                        "long_name" : "Icaraí",
                        "short_name" : "Icaraí",
                        "types" : [ "political", "sublocality", "sublocality_level_1" ]
                    },
                    {
                        "long_name" : "Niterói",
                        "short_name" : "Niterói",
                        "types" : [ "administrative_area_level_2", "political" ]
                    },
                    {
                        "long_name" : "State of Rio de Janeiro",
                        "short_name" : "RJ",
                        "types" : [ "administrative_area_level_1", "political" ]
                    },
                    {
                        "long_name" : "Brazil",
                        "short_name" : "BR",
                        "types" : [ "country", "political" ]
                    }
                ],
                "formatted_address" : "Icaraí, Niterói - State of Rio de Janeiro, 24230-090, Brazil",
                "types" : [ "postal_code" ]
            }
        ],
    }
};

export default {google};
