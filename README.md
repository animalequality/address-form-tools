## address-form-tools

Small javascript library to add geocoding feature and UI enhancements to address-related form fields.

* Adapts a zip/postal-code according to the value of a country select-list value.
  * Show/Hide zip/postal-code
  * Adapt postal-code validation pattern
  * Adapt the placeholder (todo)
* Geocodes a given (country/postal-code) tuple.
  * Could be used to store the results (eg: to complete a valid MailChimp ADDRESS field)
  * Or just react to the geocoding result to perform any arbitrary action.


## Available tools
* `countryChangeHandler`. onChange handler to attach to a country selector or DOM node.
* `postalCodeChangeHandler`. onChange handler to attach to a zip/postal-code selector or DOM node.
* `phone_field`. A wrapper for [telinput](https://intl-tel-input.com/) to bridge with the country field
* `Geocoder`. The raw geocoder for more advanced uses.

## Events and their payload
* `geocode:geocoded`. Payload: Contains the result of the `geocode()` function ie:
  * `admin1`: The geocoded state / region
  * `city`: The city (or possibly the county)

* `address-form-tools:has-postal-code`: Zip/Postal code status:
  * `show`: true|false, Whether or not a zip/postal-code is pertinent for selected country
  * `cc`: The currently selected two-letters country code
  * `value`: An optional zip/postal-code value (eg: "00000")

## Example
```js
// Defines involved DOM elements
var form = document.querySelector("form#foo"),
	c = form.querySelector("select#country"),
	p = form.querySelector("input#zip"),
	cp_container = document.querySelector("span.zipfield"),
	city = document.querySelector("form input#city"),
	state = document.querySelector("form input#state");

// Attach default behavior (placeholders / geocoding)
c.onchange = countryChangeHandler.bind(this, c, p);
p.onchange = postalCodeChangeHandler.bind(this, c, p);

// Additionally customize based on events
document.addEventListener('geocode:geocoded', (e) => {
	var data = e.detail;
	state.value = data.admin1 || '';
	city.value = data.city || '';
	document.querySelector(".geo-hint").innerHTML = [state.value, city.value].filter(e => e).join(',');
});

document.addEventListener('address-form-tools:has-postal-code', (e) => {
	cp_container.style.visibility = e.detail.show ? 'visible' : 'hidden';
});
```
